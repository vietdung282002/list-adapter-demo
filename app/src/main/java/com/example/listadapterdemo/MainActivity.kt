package com.example.listadapterdemo

import android.app.Activity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

const val TAG = "List Adapter"

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var itemAdapter: ItemAdapter
    private lateinit var editText: EditText
    private lateinit var btn: Button
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause: ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop: ")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editText = findViewById(R.id.editText)
        btn = findViewById(R.id.button2)
        val dataset = DataSource().loadText()
        recyclerView = findViewById(R.id.recycler_view)

        itemAdapter = ItemAdapter(dataset)

        recyclerView.apply {
            adapter = itemAdapter
        }

        itemAdapter.submitList(dataset)
        btn.setOnClickListener {
            val text = editText.text.toString()
            if(text != ""){
                dataset.add(0,Text(text))
                editText.getText().clear()
                itemAdapter.submitList(dataset)
                hideKeyBoard(this@MainActivity)
            }
            else{
                editText.error = "Please enter a text"
            }
        }
        Log.d(TAG, "onCreate: ")

    }
    private fun hideKeyBoard(activity: Activity?) {
        if (activity != null && activity.window != null) {
            activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }
    }
}