package com.example.listadapterdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class ItemAdapter(private val dataset: List<Text>): ListAdapter<Text, ViewHolder>(TextDiffUtil()){

    class BlueItemViewHolder(view: View): ViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.item_title)
    }

    class WhiteItemViewHolder(view: View): ViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.item_title)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        return when (viewType){
            0->BlueItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.blue_item,parent,false)
            )
            else -> WhiteItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.white_item,parent,false)
            )

        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset[position]
        if(getItemViewType(position) == 0){
            val blueHolder = holder as BlueItemViewHolder
            blueHolder.textView.text = item.text
        }else{
            val whiteHolder = holder as WhiteItemViewHolder
            whiteHolder.textView.text = item.text
        }
    }

    override fun submitList(list: MutableList<Text>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    class TextDiffUtil: DiffUtil.ItemCallback<Text>() {
        override fun areItemsTheSame(oldItem: Text, newItem: Text): Boolean {
            return newItem.text == oldItem.text
        }

        override fun areContentsTheSame(oldItem: Text, newItem: Text): Boolean {
            return oldItem == newItem
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) 0 else 1
    }
}